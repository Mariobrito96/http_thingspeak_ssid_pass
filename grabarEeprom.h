#ifndef GRABAREEPROM_H
#define GRABAREEPROM_H
#include <Arduino.h>
#include <EEPROM.h>
//defino la constante del limite de mis arreglos 
#define LIMIT_INPUT 50

class Grabareeprom
{
private:
  //variables privadas
public:
  //variables publicas
char ssidread[50];
char passread[50];
  //constructor
  Grabareeprom();
//metodos
  void eepromBegin();
  void saveSSID(String a);
  void savePassword(String b);
  String read(int addr);

};
extern Grabareeprom grabareeprom;
#endif
