//Mario Enrique Brito May 
#include <Arduino.h>
#include <Conectiontype.h>
#include <Buttoncontrol.h>
#include <Grabareeprom.h>
#include <Httpclient.h>

void setup()
{
  //inicio puerto serial
  conectiontype.conectionTypeBegin();
  //inicio el servidor de thingspeak
  httpclient.httpClientBegin();
  //llamo a la funcion para iniciar la eeprom
  grabareeprom.eepromBegin();
  //inicializo los pines de leds y boton
  buttoncontrol.buttonBegin();
}
// loop
void loop()
{
  buttoncontrol.buttonLoop();
  //condición boton
  if(buttoncontrol.getState() == true)
  {
    Serial.println("conección AP");
    conectiontype.coneccionAp();
  }
  else
  {
    if(conectiontype.wifiGetStatus()==false)//condicion para conectarse al modem (si ya se conecto no entra)
    {
      conectiontype.wifiBegin();//coneccion wifi
    }
    if(conectiontype.wifiGetStatus()==true)//condicion para conectarse al modem (valor retornado)
    {
      httpclient.httpClientLoop();
    }
  }
}
