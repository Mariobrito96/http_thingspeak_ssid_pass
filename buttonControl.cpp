#include <Buttoncontrol.h>

Buttoncontrol::Buttoncontrol(){ //LLAMO AL CONSTRUCTOR PARA QUE NO ME DE ERROR

}

void Buttoncontrol::buttonBegin()
{
  //DECLARO PINES DE ENTRADA Y SALIDA (BOTON Y LEDS)
  pinMode(0, INPUT);
  pinMode(2, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}

void Buttoncontrol::buttonLoop()
{
  buttonstate = digitalRead(0);
  if(buttonstate == 0 )
  {
    if (buttonFirsttime == false)
    {
      previousMillis = millis();
      buttonFirsttime = true;
    }
    currentMillis = millis();
    if((currentMillis-previousMillis) > 5000 && buttonFirsttime== true)
    {
      push=true;
      buttonFirsttime = false;
    }
    else
    {
      push=false;
    }
  }
  else
  {
    previousMillis = millis();
  }
}

boolean Buttoncontrol::getState()
{
  return push;
}


Buttoncontrol buttoncontrol;
