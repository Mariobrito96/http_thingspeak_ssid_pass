#include <Grabareeprom.h>
#include <Conectiontype.h>

Grabareeprom::Grabareeprom(){ //LLAMO AL CONSTRUCTOR PARA QUE NO ME DE ERROR

}

void Grabareeprom::eepromBegin()
{ //inicializo la EEPROM
  EEPROM.begin(512);
  // solicitar lo que esta guardado en la eeprom
  read(0).toCharArray(ssidread, 50);
  grabareeprom.read(50).toCharArray(passread, 50);
  Serial.println(ssidread);
  Serial.println(passread);
}

void Grabareeprom::saveSSID(String a)
{
  // Serial.println(a);
  int addr=0;
  int tamano = a.length();
  char inchar[50];
  a.toCharArray(inchar, tamano+1);//(el bufer donde copiare el string, tamaño del bufer)

  for (int i = 0; i < tamano; i++)
  {
    EEPROM.write(addr+i, inchar[i]);  //(address, valor)
  }
  for (int i = tamano; i < LIMIT_INPUT; i++)
  {
    EEPROM.write(addr+i, 255);//los bytes son 255 cuando no hay nada en la eeprom
  }
  EEPROM.commit();
}

void Grabareeprom::savePassword(String b)
{
  // Serial.println(b);
  int addr=50;
  int tamano = b.length();
  char inchar[50];
  b.toCharArray(inchar, tamano+1);//(el bufer donde copiare el string, tamaño del bufer)
  for (int i = 0; i < tamano; i++)
  {
    EEPROM.write(addr+i, inchar[i]);
  }
  for (int i = tamano; i < LIMIT_INPUT; i++)
  {
    EEPROM.write(addr+i, 255);
  }
  EEPROM.commit();
}

String Grabareeprom::read(int addr)
{
  byte lectura;
  String strlectura = "";
  for (int i = addr; i < addr+LIMIT_INPUT; i++)
  {
    lectura = EEPROM.read(i);
    if (lectura != 255 && lectura != 0)
    {
    strlectura += (char)lectura;
    }
  }
  return strlectura;
}

Grabareeprom grabareeprom;
