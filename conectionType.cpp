#include <Conectiontype.h>

ESP8266WebServer server(80);


Conectiontype::Conectiontype(){ //LLAMO AL CONSTRUCTOR PARA QUE NO ME DE ERROR

}

void Conectiontype::conectionTypeBegin()
{
  Serial.begin(115200);
  Serial.println();
}

void Conectiontype::wifiBegin()
{
  //solicito los datos para conectarme
  grabareeprom.read(0).toCharArray(ssidread, 50);
  grabareeprom.read(50).toCharArray(passread, 50);
  //convierto de char a string
  ssid = String(ssidread);
  password= String(passread);
  delay(3000);
  if (WiFi.status() != WL_CONNECTED)
  {
    Serial.print(".");
    delay(500);
    WiFi.mode(WIFI_STA);       //para que no inicie el SoftAP en el modo normal
    WiFi.begin(ssid, password);//mystringvariable.c_str() convierte un string a char
  }

  if (WiFi.status() == WL_CONNECTED)
  {
    Serial.println("");
    Serial.println("WiFi connected");
    wififlag=true;
  }
}

boolean Conectiontype::wifiGetStatus()
{
  return wififlag;
}

void Conectiontype::coneccionAp()
{
  conectiontype.serverOn();
  digitalWrite(2, HIGH);
  Serial.print("Setting soft-AP ... ");
  // me conecto como punto de aceso
  boolean result = WiFi.softAP(ssidAP);//mando nombre de la red creada, se le puede añadir la contraseña
  if(result)
  {
    Serial.println("Ready");
  }
  else
  {
    Serial.println("Failed!");
  }
  IPAddress myIP = WiFi.softAPIP();
  Serial.print("IP:");
  Serial.println(myIP);
  while(1){
    conectionTypeLoop();
  }
}

void Conectiontype::serverOn()
{
  server.on("/config", std::bind(&Conectiontype::sendData, this));
  server.begin();
  Serial.println("HTTP server started");
}

void Conectiontype::sendData()
{
  grabareeprom.saveSSID(conectiontype.ssidValue());
  grabareeprom.savePassword(conectiontype.passValue());
  grabareeprom.read(0).toCharArray(ssidread, 50);
  grabareeprom.read(50).toCharArray(passread, 50);
  Serial.println(ssidread);
  Serial.println(passread);
  delay (500);
  ESP.restart();
  //reset
}

void Conectiontype::conectionTypeLoop()
{
  server.handleClient();
}

String Conectiontype::ssidValue()
{
  String ssid= server.arg("ssid");
  return ssid;
}

String Conectiontype::passValue()
{
  String pass= server.arg("pass");
  return pass;
}

Conectiontype conectiontype;
