#ifndef CONECTIONTYPE_H
#define CONECTIONTYPE_H
#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <Grabareeprom.h>

class Conectiontype
{
private:
//variables privadas
  String ssidAP = "redbrito ";
  boolean wififlag=false;
  char ssidread[50];
  char passread[50];
  String ssid;
  String password;
public:
  //variables publicas
Conectiontype();//constructor
//metodos
void conectionTypeBegin();
void conectionTypeLoop();
void wifiBegin();
void coneccionAp();
void serverOn();
void sendData();
boolean wifiGetStatus();
String ssidValue();
String passValue();

};
extern Conectiontype conectiontype;
#endif
