#ifndef BUTTONCONTROL_H
#define BUTTONCONTROL_H
#include <Arduino.h>
#include <Conectiontype.h>

class Buttoncontrol
{
private:
  //variables privadas
int buttonstate;
unsigned long previousMillis;
unsigned long currentMillis;
boolean buttonFirsttime= false;
public:
  //variables publicas
boolean push= false;
Buttoncontrol();//constructor
//metodos
void buttonBegin();
void buttonLoop();
boolean getState();
};
extern Buttoncontrol buttoncontrol;
#endif
